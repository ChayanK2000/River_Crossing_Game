import pygame
import sys
import random
import math
from random import randint, choice
from config import *

screen = pygame.display.set_mode((800, 600))

pygame.display.set_caption("River Crossing")
icon = pygame.image.load('river.png')
pygame.display.set_icon(icon)

player_man = pygame.image.load('run.png')
player_man = pygame.transform.scale(player_man, (35, 35))
player_x = 365
player_y = 565
player_x_ch = 0
player_y_ch = 0
count1 = 0

player = player_man
arr_m = [0] * 600
arr_f = [0] * 600

player_woman = pygame.image.load('runWoman.png')
player_woman = pygame.transform.scale(player_woman, (35, 35))
count2 = 0

score1 = []
score2 = []

ship = []
ship_x = []
ship_y = []
n_ships = 10
shipimg = pygame.transform.scale(pygame.image.load('ship.png').convert_alpha(), (40, 40))

n_fix_obs = 10

lion = pygame.image.load('lion.png')
lion = pygame.transform.scale(lion, (45, 45))
automobile = pygame.image.load('automobile.png')
automobile = pygame.transform.scale(automobile, (45, 45))
barrier = pygame.image.load('barrier.png')
barrier = pygame.transform.scale(barrier, (45, 45))

fix_obs = [lion, lion, lion, lion, barrier, barrier, barrier, automobile, automobile, automobile]
fix_obs_x = [0, 400, 700, 100, 250, 150, 600, 750, 10, 361]
fix_obs_y = [0, 111, 555, 444, 111, 222, 333, 222, 333, 444]
for i in fix_obs_y:
    arr_f[i] += 1

score = 0
score_x = 650
score_y = 10
elapsed_time = 0
elapsed_time1 = 0
elapsed_time2 = 0
time1 = []
time2 = []

colours = [WHITE, RED, GREEN, BLUE, YELLOW]
xyz = 0

pl_change = 1


def display(score, x, y):
    if (player == player_man):
        score = 150 - score
    screen.blit(font_text.render("SCORE: " + str(score), True, WHITE), (x, y))


def display_final():
    screen.blit(font_text.render("ROUND1: ", True, WHITE), (300, 100))
    screen.blit(
        font_text.render("PLAYER1: " + str(150 - score1[0]) + "points " + str(time1[0]) + "s", True, WHITE),
        (300, 130))
    screen.blit(font_text.render("PLAYER2: " + str(score2[0]) + "points " + str(time2[0]) + "s", True, WHITE),
                (300, 160))
    screen.blit(font_text.render("ROUND2: ", True, WHITE), (300, 200))
    screen.blit(
        font_text.render("PLAYER1: " + str(150 - score1[1]) + "points " + str(time1[1]) + "s", True, WHITE),
        (300, 230))
    screen.blit(font_text.render("PLAYER2: " + str(score2[1]) + "points " + str(time2[1]) + "s", True, WHITE),
                (300, 260))
    display_winner()


def display_winner():
    global xyz
    final1 = (150 - score1[0] + 150 - score1[1] - 2 * (time1[0] + time1[1]))
    final2 = (score2[0] + score2[1] - 2 * (time2[0] + time2[1]))
    if final1 > final2:
        screen.blit(res_text.render("PLAYER1 WINS!!!", True, colours[xyz % 5]), (300, 300))
    elif final1 < final2:
        screen.blit(res_text.render("PLAYER2 WINS!!!", True, colours[xyz % 5]), (300, 300))
    else:
        screen.blit(res_text.render("It's A DRAW!!!", True, colours[xyz % 5]), (320, 300))
    xyz += 1
    screen.blit(font_text.render("Press Any Key To EXIT", True, WHITE), (600, 550))


def display_time(elapsed_time):
    screen.blit(font_text.render("TIME: " + str(elapsed_time), True, WHITE), (650, 30))


for i in range(n_ships):
    ship.append(shipimg)
    ship_x.append(random.randint(0, 800))
    random_y = (choice(
        [random.randint(55, 55), random.randint(166, 166), random.randint(277, 277), random.randint(388, 388),
         random.randint(499, 499)]))
    arr_m[random_y] += 1
    ship_y.append(random_y)
    ship_x_change = 1.1

obs = fix_obs + ship
obs_x = fix_obs_x + ship_x
obs_y = fix_obs_y + ship_y


def obstacles(i):
    screen.blit(obs[i], (obs_x[i], obs_y[i]))


def partition():
    pygame.draw.rect(screen, SAND, (0, 0, 800, 45))
    pygame.draw.rect(screen, SAND, (0, 111, 800, 45))
    pygame.draw.rect(screen, SAND, (0, 222, 800, 45))
    pygame.draw.rect(screen, SAND, (0, 333, 800, 45))
    pygame.draw.rect(screen, SAND, (0, 444, 800, 45))
    pygame.draw.rect(screen, SAND, (0, 555, 800, 45))


def player_pos(x, y):
    screen.blit(player, (x, y))


def collision(i, px, py):
    dist = math.sqrt(math.pow((obs_x[i] - px), 2) + math.pow((obs_y[i] - py), 2))
    if dist <= 39:
        return True
    else:
        return False


running = True
while running:
    if player == player_man:
        up = pygame.K_UP
        down = pygame.K_DOWN
        left = pygame.K_LEFT
        right = pygame.K_RIGHT

    if player == player_woman:
        up = pygame.K_w
        down = pygame.K_s
        left = pygame.K_a
        right = pygame.K_d
    if pl_change == 1:
        player_x_ch = 0
        player_y_ch = 0
        pl_change = 0
    screen.fill(SEA)
    partition()

    if (count2 == 1):
        ship_x_change = 2
    if (count2 == 2):
        while running:
            screen.fill((0, 0, 0))
            display_final()
            for event in pygame.event.get():

                if event.type == pygame.KEYDOWN or event.type == pygame.QUIT:
                    running = False
                    break
            pygame.display.update()
        break

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False  # or "running=False"

        if event.type == pygame.KEYDOWN:
            if event.key == left:
                player_x_ch = -1
            if event.key == right:
                player_x_ch = 1
            if event.key == up:
                player_y_ch = -1
            if event.key == down:
                player_y_ch = 1
        if event.type == pygame.KEYUP:
            if ((event.key == left) or (event.key == right)):
                player_x_ch = 0
            if ((event.key == up) or (event.key == down)):
                player_y_ch = 0

    player_x += player_x_ch
    if (player_x >= (800 - 35)):
        player_x = (800 - 35)
    if (player_x <= 0):
        player_x = 0

    if (player == player_man):

        player_y += player_y_ch
        if (player_y >= (600 - 35)):
            player_y = (600 - 35)
        if (player_y <= 0):
            player_y = 0
            player = player_woman
            player_x = 365
            player_y = 0
            count1 += 1;
            pl_change = 1
            elapsed_time1 = pygame.time.get_ticks() // 1000

            if (count1 <= 2):
                score1.append(score)
                time1.append(elapsed_time1 - elapsed_time)
            elapsed_time = elapsed_time1
            score = 0
    if (player == player_woman):

        player_y += player_y_ch
        if (player_y >= (600 - 35)):
            player_y = (600 - 35)
            player = player_man
            player_x = 365
            player_y = 565
            count2 += 1;
            elapsed_time2 = pygame.time.get_ticks() // 1000
            pl_change = 1
            if (count2 <= 2):
                score2.append(score)
                time2.append(elapsed_time2 - elapsed_time)
            elapsed_time = elapsed_time2
            score = 0
        if (player_y <= 0):
            player_y = 0

    if (player == player_man):

        score = 0
        for k in range(player_y + 35):
            if player_y + 35 > 600:
                break
            score += (10 * arr_m[k])
            score += (5 * arr_f[k])

    if (player == player_woman):

        score = 0
        for k in range(player_y - 35):
            if player_y - 35 < 0:
                break
            score += (10 * arr_m[k])
            score += (5 * arr_f[k])

    for j in range(n_ships + n_fix_obs):
        if j >= 10 and j <= 19:
            obs_x[j] = (obs_x[j] + ship_x_change) % 800

        if (collision(j, player_x, player_y)):
            if (player == player_man):
                player = player_woman
                player_x = 365
                player_y = 0
                count1 += 1
                elapsed_time1 = pygame.time.get_ticks() // 1000
                pl_change = 1

                if (count1 <= 2):
                    score1.append(score)
                    time1.append(elapsed_time1 - elapsed_time)

                elapsed_time = elapsed_time1
                score = 0
            else:
                player = player_man
                player_x = 365
                player_y = 565
                count2 += 1
                elapsed_time2 = pygame.time.get_ticks() // 1000
                pl_change = 1

                if (count2 <= 2):
                    score2.append(score)
                    time2.append(elapsed_time2 - elapsed_time)

                elapsed_time = elapsed_time2
                score = 0
            break

        obstacles(j)

    display_time(pygame.time.get_ticks() // 1000 - elapsed_time)
    display(score, score_x, score_y)
    player_pos(player_x, player_y)
    pygame.display.update()
