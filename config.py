import pygame

pygame.init()

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
SAND = (255, 255, 51)
SEA = (70, 150, 255)

font_text = pygame.font.Font('freesansbold.ttf', 15)
res_text = pygame.font.Font('freesansbold.ttf', 32)
