Hello!!!
Glad You Stopped By!

This is my first game and that too in python - River Crossing.
In the game you(player) has to cross the river without hitting obstacles.

Its a two player game.
When one of the player dies or reaches the other end, its time for other player to play from the opposite bank.
Two such rounds will be played and then the winner wold be decided.
You get 5 points for crossing fixed obstacles and 10 points for crossing moving obstacles.

[NOTE:]
1. The fxed Obstacles always appear in the same position whenever the game is played, BUT, the moving obstacles appear randomly each time the game is played. Hence no one can premeditate his strategy every time, thus giving you a fair chance.
2. As they appear random, it has its own pros and cons, like if more ships are just one row above you either you wud score low or even just crossing one row would fetch you many points...Thus making it more Exciting!!
3.After the game is over The Scoreboard is listed on the screen- showing the points accrued as well as the time taken(both are showed while playing as well)
4.YES! TIME does play a big role, i.e, there's penalty for taking time as well.
	Twice the time taken by you would be deducted from the score accrued. Hence play smartly and fast, because even such a scenario can happen that a player reaches the other side while the second player doesn't, still player2 wins because he was way faster!!
5. Also if a player crosses a row and comes down again his score decreases as well, hence play intelligently.
6. Also after every round the speed of ships increase thus increasing the difficulty.6. Also after every round the speed of ships increase thus increasing the difficulty.6. Also after every round the speed of ships increase thus increasing the difficulty.6. Also after every round the speed of ships increase thus increasing the difficulty.6. Also after every round the speed of ships increase thus increasing the difficulty.6. Also after every round the speed of ships increase thus increasing the difficulty.

CONTROLS:

PLAYER1(Denoted as a picture of man):
up----UP ARROW KEY
down----DOWN ARROW KEY
left----LEFT ARROW KEY
right----RIGHT ARROW KEY

PLAYER2(Denoted as picture of woman):
up----W
down----A
left----S
right----D
